# mondumo: a conquerable verse

These documents describe what a simplier, more perfect world might be.
We, by no means, believe to have all of the answers (but we won't be remiss in trying).

We encourage input from every source.
You're bound to find something to improve;
when you do, let us know!

We have

 - an [omnibus]() of motivations,
 - a [manifesto]() to declare yourself,
 - a [catalog]() of congruent ideas,
 - a [license]() to create objects,
 - an [emblem]() to promote the project,
 - . . .

# contributing
