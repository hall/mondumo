# etalono

A simple system of measurement based on the properties of nature.

## digits

The twelve **digits** (in ascending value) are

    0. zero
    1. un
    2. du
    3. tri
    4. quar
    5. kin
    6. sis
    7. sep
    8. ok
    9. non
    ↊. dek
    ↋. el

## number

A **number** is a sequence of digits with an optional *radix point* (.) separating the whole part from the fractional part.

The value of a digit is determined by its position within a number;
such that (if $B$ is the base):

$$
\cdots ab.cd \cdots = \cdots + (a * B^1) + (b * B^0) + (c * B^-1) + (d * B^-2) + \cdots
$$

For example,

$$
8↊2.35 = (8 * 10^2) + (↊ * 10^1) + (2 * 10^0) + (3 * 10^-1) + (5 * 10^-2)
$$

### shorthand

Eventually, numbers will become unwieldly in notation.
To mitigate this issue, we create a shorthand by taking advantage of the positional numbering system.

#### exponential operator

The notation

$$
NeP
$$

denotes a number _N_ raised to the power _P_; for example,

$$
5.2e3 = 5.2 \cdot 10^3 = 5200.
$$

This precludes the need to memorize arbitrary prefixes (as is done in, for example, the metric system).


#### radix operator

the notation

$$
BrN
$$

denotes the number _N_ written in the base _B_; for example,

$$
↊r12 = 10
$$

is the number `12` in base `↊` (_ten_).
Without an explicit radix, numbers are assusmed to be in base `10` (_dozen_).

## fundamental units

Insofar as we know, there exists many fundamental constants of our universe -- some of which include:

 * speed of light ($c$ from special relativity)
 * gravitational constant ($G$ from general relativity)
 * Planck constant ($bar h$ from quantum mechanics)
 * Coulomb constant ($k_e$ from electromagnetism)
 * Boltzmann constant ($k_b$ from thermodynamics)

Setting each of these to unity yields a "natural" system of units that is independent of many human constructs and their anthropocentric arbitrariness.


### length (L)


### maso (M)


### tempo (T)

Two useful units of time

 - the rotation of the Earth on its axis
 - the revolution of the Earth around the Sun

are fixed to lend a great deal of practicality to this system of timekeeping.

WARNING: The unfortunate reconsiliation of these two facets creates a great deal of complexity;
         rendering a simple solution remains a high priority.

These restrictions form a perennial *year* of 265 (↊r365) *days*;
which are partitioned into 10 (nameless) *months* beginning near the winter solstice of the northern hemisphere.
The year 0 is assigned to the beginning of the Holocene period (~↊r11700 B.C.).


#### notation

The canonical format is

    yyyy.m|dd.hhmmss

For example,

    7↋31.↋|1.59↋16

is 7↋31.↊ years and 1.59↋16 days (5 'hours' and 9↋ 'minutes' and 16 'seconds', if you will).

There are no timezones or daylight saving time; i.e., all of the world observes a single time.

The year ↊r2018 A.D. is 7↋32.


#### algorithm

To convert conventional time to standard time,

 1. get current universal time (UTC)
 2. move year to holocene period
 3. convert time to decimal notation
 4. sum the components
 5. convert to dozenal

e.g. (in Javascript),

    setInterval( function () {
        var now = new Date();

        // translate year to the holocene period and then convert to dozenal
        var year   = (now.getUTCFullYear() + 11700).toString(12);
        var month  =  now.getUTCMonth().toString(12);
        var day    =  now.getUTCDate();
        var time   =  now.getUTCHours()/2      + now.getUTCMinutes()/120
                   +  now.getUTCSeconds()/7200 + now.getUTCMilliseconds()/7200000;
        time = time.toString(12).substring(0,6).replace(/\./g, '');
    
        dozenal = year+'.'+month + '|' + day+'.'+time;
    
        // character replacements for personal preference
        dozenal = dozenal.replace(/a/g, '↊');
        dozenal = dozenal.replace(/b/g, '↋');
    
        document.getElementById('clock').lastChild.innerHTML = dozenal;
    }, 72/250);

### charge (Q)


### temperature (K)


## derived units

From these three fundamental units, all others can be derived (that is, all other units are merely short-handed methods to write the same thing).

|                   |  M  |  L  |  T  |  Q  |  K  |
|------------------:|-----|-----|-----|-----|-----|
|area               |  0  |  2  |  0  |  0  |  0  |
|volume             |  0  |  3  |  0  |  0  |  0  |
|momentum           |  1  |  1  | -1  |  0  |  0  |
|energy             |  1  |  2  | -2  |  0  |  0  |
|force              |  1  |  1  | -2  |  0  |  0  |
|power              |  1  |  2  |  3  |  0  |  0  |
|density            |  1  | -3  |  0  |  0  |  0  |
|energy density     |  1  | -1  | -2  |  0  |  0  |
|intensity          |  1  |  0  | -3  |  0  |  0  |
|angular frequency  |  0  |  0  | -1  |  0  |  0  |
|pressure           |  1  | -1  | -2  |  0  |  0  |
|current            |  0  |  0  |  1  | -1  |  0  |
|voltage            |  1  |  2  | -3  |  0  |  0  |
|impedance          |  1  |  2  | -1  | -2  |  0  |
|acceleration       |  0  |  1  | -2  |  0  |  0  |

